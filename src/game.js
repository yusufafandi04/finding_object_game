/*global mainmenu*/
/*global ingame*/
/*global Phaser*/
/*global winScreen*/

var game = new Phaser.Game(1366, 768, Phaser.AUTO, 'game-canvas');
var scoreUser = 0;
var timerUser = 0;
var isWin = false;
var gameLimit = 0;
var maxWidth = 1600;
var maxHeight = 900;
var totalLevel = 5;
var currentLevel = 0;
var key_;
var id_;
var totalKoin_;
var scaleMode = Phaser.ScaleManager.SHOW_ALL;
var isOneCycle = false;

WebFontConfig = {

    //  'active' means all requested fonts have finished loading
    //  We set a 1 second delay before calling 'createText'.
    //  For some reason if we don't the browser cannot render the text the first time it's created.
    active: function() { game.time.events.add(Phaser.Timer.SECOND, loadGame, this); },

    //  The Google Fonts we want to load (specify as many as you like in the array)
     google: {families :[ 'Lato:300,400:latin']},  // NEW UPDATE PIXELNINE



};
function loadGame(){};

game.state.add("LevelGenerator",levelGenerator);
game.state.add("MainMenu",mainmenu);
game.state.add("Ingame",ingame);
game.state.add("WinScreen",winScreen);
