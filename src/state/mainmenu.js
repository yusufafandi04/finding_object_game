/*global Phaser */
var mainmenu = function(game){
	this.game = game;
};

mainmenu.prototype = {
	preload: function(){
		this.game.scale.maxWidth = screen.innerWidth;
		this.game.scale.maxHeight = window.innerHeight;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.atlasJSONHash('SplashScreen', 'asset/SplashScreen/SplashScreen.png', 'asset/SplashScreen/SplashScreen.json');
	},
	create: function(){
		var background = this.game.add.sprite(0,0,'SplashScreen','background splash.jpg');
		background.width = this.game.width;
		background.height = this.game.height;

		var startButton = this.game.add.sprite(0,0,'SplashScreen','btn mulai main.png');
  	    // startButton.x = (this.game.width-startButton.width)/2;
        startButton.x = 88; // NEW UPDATE PIXELNINE
		startButton.y = this.game.height - this.game.height/4;
		startButton.inputEnabled = true;
		startButton.events.onInputDown.add(this.nextState, this);
	},
	nextState: function(){
		if(!isOneCycle){
			var ajaxReq = $.ajax({
				type: 'GET',
				url: apiUrl+"/api/ho/game/score",
				dataType: "JSON",
				success: function(resultData) {
					if(resultData.status == false)
					{
						alert("ERROR");
					}else{
						if(resultData.data.level.length==totalLevel)
						{
							currentLevel = 0;
							isOneCycle = true;
						}else{
							currentLevel = resultData.data.level.length;
						}

						//-------------START AJAX AREA--------------------//
						ajaxReq = $.ajax({
							type: 'POST',
							url: apiUrl+"/api/ho/game",
							dataType: "JSON",
							data:{
								level:currentLevel
							},
							success: function(resultData) {
								console.log(JSON.stringify(resultData));

								if(resultData.status == false)
								{
									alert("ERROR");
								}else{
									key_ = resultData.data.key;
									id_ = resultData.data.id;
									totalKoin_ = resultData.data.total_coin;
									game.state.start("Ingame");
								}
							}
						});
						//-------------END AJAX AREA--------------------//
					}
				}
			});
			//-----------------END AJAX AREA----------------------//
			ajaxReq.error(function() { alert("Data Error"); });
		}else{
			ajaxReq = $.ajax({
				type: 'POST',
				url: apiUrl+"/api/ho/game",
				dataType: "JSON",
				data:{
					level:currentLevel
				},
				success: function(resultData) {
					console.log(JSON.stringify(resultData));

					if(resultData.status == false)
					{
						alert("ERROR");
					}else{
						key_ = resultData.data.key;
						id_ = resultData.data.id;
						totalKoin_ = resultData.data.total_coin;
						game.state.start("Ingame");
					}
				}
			});
		}
	}
}
