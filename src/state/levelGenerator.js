var totalBarang = 50;
var objectToFind = 10;
var levelTitle = "Level 1";
var levelName = "levelName";
var stringName = ["MAJALAH","BAJU","REMOTE","TISU","KAOS KAKI","HANDUK","KOPER","KUNCI","TOPI","DOMPET","PASPOR","KACAMATA","SENDAL","SMARTPHONE","TAS","SEPATU","LAPTOP","JAM TANGAN","NOTEBOOK","PAYUNG","SENTER","HEADPHONE","LAMPU","WEKER","KARTU","GELAS","MANGKOK","BOTOL","KESET","POT","BEBEK","TEMPAT SAMPAH","BALON","KIPAS","JAM","STICK","PENA","TABUNGAN","DASI","TEMPAT LILIN","LIPSTIK","HANDYCAM","GELANG","SISIR","BOHLAM","VAS","RADIO","KAMERA","PANCI","PISANG"];
var folder_dest = "Level Coffee Shop";
var hint = "temukan barang yang mengandung unsur sara";

var allRandomBarang = [];
var levelGenerator = function(game) {
    console.log("%cStarting my awesome game", "color:white; background:red");
};


levelGenerator.prototype = {
    preload : function(){
        this.ingameData = levelSetting[currentLevel];
        console.log(this.ingameData);
        game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
        
        this.game.load.atlasJSONHash('GAME_ASSET','asset/'+folder_dest+'/JsonData.png','asset/'+folder_dest+'/JsonData.json')
        
        this.game.load.image("black_bg", "asset/image/Solid_black.png");
        this.game.load.image("background", "asset/level 1/background.jpg");
        this.game.load.image("button", "asset/image/blue_button00.png");
    },
    create : function(){
        var background = game.add.sprite(0, 0, 'GAME_ASSET','background.jpg');
        background.width = game.width;
        background.height = game.height;
        
        var buttonRenderText = game.add.sprite(game.width/2,game.height-80,"button");
        buttonRenderText.inputEnabled = true;
        buttonRenderText.events.onInputDown.add(this.renderString, this);
        for(var i=0;i<totalBarang;i++)
        {
            var tempImage = game.add.sprite(i*20,20,"GAME_ASSET",(i+1)+".png");
            //tempImage.x = this.ingameData.objectData[i].posX;
            //tempImage.y = this.ingameData.objectData[i].posY;
            
            tempImage.inputEnabled = true;
            tempImage.input.enableDrag();
            allRandomBarang.push(tempImage);
        }
        
    },
    renderString : function()
    {
        var consoleText = {};
        consoleText.levelTitle = levelTitle;
        consoleText.levelName = levelName;
        consoleText.hint = hint;
        consoleText.folder_dest = folder_dest;
        consoleText.objectToFind = objectToFind;
        consoleText.objectData = [];
        for(var i=0;i<totalBarang;i++)
        {
            var tempObjData = {};
            tempObjData.name = stringName[i];
            tempObjData.posX = allRandomBarang[i].x;
            tempObjData.posY = allRandomBarang[i].y;
            consoleText.objectData.push(tempObjData);
        }
        console.log(JSON.stringify(consoleText));
    }
}